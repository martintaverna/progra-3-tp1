package grafico;

import java.awt.EventQueue;

import javax.swing.JFrame;

import negocio.Tablero;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;

public class Juego {

	private JFrame frame;
	private Tablero tablero;
	private JButton boton1;
	private JButton boton2;
	private JButton boton3;
	private JButton boton4;
	private JButton boton5;
	private JButton boton6;
	private JButton boton7;
	private JButton boton8;
	private JButton boton9;
	private JButton boton10;
	private JButton boton11;
	private JButton boton12;
	private JButton boton13;
	private JButton boton14;
	private JButton boton15;
	private JButton boton16;
	private JPanel cartelVictoria;
	private JLabel numeroTurnos;
	private JLabel numeroTurnos2;
	private int turnos;
	private JLabel lblGanaste;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Juego window = new Juego();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Juego() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.tablero = new Tablero();
		this.turnos = 0;
		JButton[] botones = new JButton[16];
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Inicializacion de los botones
		this.boton1 = new JButton();
		boton1.setBounds(205, 150, 85, 85);
		
		this.boton2 = new JButton();
		boton2.setBounds(306, 150, 85, 85);
		
		this.boton3 = new JButton();
		boton3.setBounds(407, 150, 85, 85);
		
		this.boton4 = new JButton();
		boton4.setBounds(508, 150, 85, 85);
		
		this.boton5 = new JButton();
		boton5.setBounds(205, 250, 85, 85);
		
		this.boton6 = new JButton();
		boton6.setBounds(306, 250, 85, 85);
		
		this.boton7 = new JButton();
		boton7.setBounds(407, 250, 85, 85);
		
		this.boton8 = new JButton();
		boton8.setBounds(508, 250, 85, 85);
		
		this.boton9 = new JButton();
		boton9.setBounds(205, 350, 85, 85);
		
		this.boton10 = new JButton();
		boton10.setBounds(306, 350, 85, 85);
		
		this.boton11 = new JButton();
		boton11.setBounds(407, 350, 85, 85);
		
		this.boton12 = new JButton();
		boton12.setBounds(508, 350, 85, 85);
		
		this.boton13 = new JButton();
		boton13.setBounds(205, 450, 85, 85);
		
		this.boton14 = new JButton();
		boton14.setBounds(306, 450, 85, 85);
		
		this.boton15 = new JButton();
		boton15.setBounds(407, 450, 85, 85);
		
		this.boton16 = new JButton();
		boton16.setBounds(508, 450, 85, 85);
		
		//Seteos del frame
		
		botones[0] = this.boton1;
		botones[1] = this.boton2;
		botones[2] = this.boton3;
		botones[3] = this.boton4;
		botones[4] = this.boton5;
		botones[5] = this.boton6;
		botones[6] = this.boton7;
		botones[7] = this.boton8;
		botones[8] = this.boton9;
		botones[9] = this.boton10;
		botones[10] = this.boton11;
		botones[11] = this.boton12;
		botones[12] = this.boton13;
		botones[13] = this.boton14;
		botones[14] = this.boton15;
		botones[15] = this.boton16;
		
		this.domarBotones(botones);
		
		
		//Cartel de victoria y juegar de vuelta
		this.cartelVictoria = new JPanel();
		this.mostrarUOcultar(cartelVictoria, false);
		cartelVictoria.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(cartelVictoria);
		cartelVictoria.setLayout(null);
		
		JButton jugarOtra = new JButton();
		jugarOtra.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		jugarOtra.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/BotonJugar.png")));
		jugarOtra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				jugarDeVuelta();
			}
		});
		jugarOtra.setBounds(20, 490, 300, 100);
		this.domarBoton(jugarOtra);
		
		JButton salir = new JButton();
		salir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		salir.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/BotonSalir.png")));
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frame.dispose();
			}
		});
		salir.setBounds(490, 490, 300, 100);
		this.domarBoton(salir);
		cartelVictoria.add(jugarOtra);
		cartelVictoria.add(salir);
		
		this.numeroTurnos2 = new JLabel();
		numeroTurnos2.setForeground(Color.BLACK);
		numeroTurnos2.setHorizontalTextPosition(SwingConstants.CENTER);
		numeroTurnos2.setFont(new Font("Speed Racing", Font.PLAIN, 90));
		numeroTurnos2.setBounds(370, 85, 150, 150);
		cartelVictoria.add(numeroTurnos2);
		
		this.lblGanaste = new JLabel();
		lblGanaste.setBounds(0, 0, 800, 600);
		cartelVictoria.add(lblGanaste);
				
		//Fin del cartel de victoria
		
		this.mostrarBotones(botones);
		frame.getContentPane().setLayout(null);
		
		//Action listeners
		
		boton1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(0, 0);
			}
		});
		
		boton2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(0, 1);
			}
		});
		
		boton3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(0, 2);
			}
		});
		
		boton4.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(0, 3);
			}
		});
		
		boton5.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(1, 0);
			}
		});
		
		boton6.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(1, 1);
			}
		});
		
		boton7.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(1, 2);
			}
		});
	
		boton8.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(1, 3);
			}
		});
		
		boton9.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(2, 0);
			}
		});
		
		boton10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				jugar(2, 1);
			}
		});
		
		boton11.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(2, 2);
			}
		});
		
		boton12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				jugar(2, 3);
			}
		});
		
		boton13.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(3, 0);
			}
		});
		
		boton14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				jugar(3, 1);
			}
		});
		
		boton15.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(3, 2);
			}
		});
		
		boton16.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				jugar(3, 3);
			}
		});
		
		//Fin de los botones
		
		//C�digo encargado de mostrar los turnos.
		this.numeroTurnos = new JLabel(Integer.toString(turnos));
		numeroTurnos.setForeground(Color.BLACK);
		numeroTurnos.setHorizontalTextPosition(SwingConstants.CENTER);
		numeroTurnos.setFont(new Font("Speed Racing", Font.PLAIN, 72));
		numeroTurnos.setBounds(660, 260, 129, 84);
		frame.getContentPane().add(numeroTurnos);
		
		JLabel textoTurno = new JLabel ("Turnos:");
		textoTurno.setForeground(Color.BLACK);
		textoTurno.setFont(new Font("Speed Racing", Font.PLAIN, 60));
		textoTurno.setBounds (605, 130, 200, 200);
		frame.getContentPane().add(textoTurno);
		
		//C�digo encargado de mostrar el fondo.
		JLabel fondo = new JLabel();
		fondo.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/Fondo juego.jpg")));
		fondo.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(fondo);
		
		JLabel awayWeGo = new JLabel("\r\n");
		awayWeGo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				fondo.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/Fondo juego2.jpg")));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				fondo.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/Fondo juego.jpg")));
			}
		});
		awayWeGo.setBounds(121, 34, 546, 47);
		frame.getContentPane().add(awayWeGo);
		
		refrescar();	
	}
	

	void refrescar()
	{
		asignarColor(tablero.getValor(0, 0), boton1);
		asignarColor(tablero.getValor(0, 1), boton2);
		asignarColor(tablero.getValor(0, 2), boton3);
		asignarColor(tablero.getValor(0, 3), boton4);
		asignarColor(tablero.getValor(1, 0), boton5);
		asignarColor(tablero.getValor(1, 1), boton6);
		asignarColor(tablero.getValor(1, 2), boton7);
		asignarColor(tablero.getValor(1, 3), boton8);
		asignarColor(tablero.getValor(2, 0), boton9);
		asignarColor(tablero.getValor(2, 1), boton10);
		asignarColor(tablero.getValor(2, 2), boton11);
		asignarColor(tablero.getValor(2, 3), boton12);
		asignarColor(tablero.getValor(3, 0), boton13);
		asignarColor(tablero.getValor(3, 1), boton14);
		asignarColor(tablero.getValor(3, 2), boton15);
		asignarColor(tablero.getValor(3, 3), boton16);
		this.numeroTurnos.setText(Integer.toString(turnos));
		this.gano();
	}
	
	void asignarColor(boolean luz, JButton boton)
	{
		if (luz)
			boton.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/prendiu.png")));
		else
			boton.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/apagao.png")));
	}
	
	void gano()
	{
		if (tablero.gano())
		{
			if(this.turnos <= 3)
			{
				this.lblGanaste.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/PrimeraPosicion.jpg")));
			}
			else if (this.turnos <=6)
			{
				this.lblGanaste.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/SegundaPosicion.jpg")));
			}
			else
			{
				this.lblGanaste.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/TerceraPosicion.jpg")));
			}
			this.numeroTurnos2.setText(Integer.toString(turnos));
			this.mostrarUOcultar(cartelVictoria, true);
		}
	}
	
	void mostrarUOcultar (JPanel panel, boolean a)
	{
		panel.setEnabled(a);
		panel.setVisible(a);
	}
	
	void jugar(int i, int j)
	{
		if (!tablero.gano())
		{
			tablero.jugada(i, j);
			turnos++;
			this.refrescar();
		}	
	}
	
	void jugarDeVuelta()
	{
		tablero.reiniciar();
		turnos = 0;
		refrescar();
		mostrarUOcultar(cartelVictoria, false);
	}
	
	void domarBotones(JButton[] botones)
	{
		for(int i = 0; i < botones.length; i++)
		{
			domarBoton(botones[i]);		
		}
	}
	
	void domarBoton(JButton boton)
	{
		boton.setBackground(null);
		boton.setBorder(null);
		boton.setContentAreaFilled(false);
	}
	
	void mostrarBotones(JButton[] botones)
	{
		for(int i = 0; i < botones.length; i++)
			frame.getContentPane().add(botones[i]);
	}
}
