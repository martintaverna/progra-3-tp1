package negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TableroTest {

	@Test
	void testTablerosAleatorios() 
	{
		Tablero tablero1 = new Tablero();
		Tablero tablero2 = new Tablero();
		assertNotEquals(tablero1, tablero2);
	}
	
	@Test
	void testFinDelJuego()
	{
		Tablero tablero1 = new Tablero();
		for (int i = 0; i < tablero1.getTama�o(); i++)
		{
			for(int j = 0; j < tablero1.getTama�o(); j++)
			{
				tablero1.setPosicion(i, j, false);
			}
		}
		assertTrue(tablero1.gano());
	}
	
	@Test
	void testReiniciarTablero()
	{
		Tablero tablero1 = new Tablero();
		Tablero auxiliar;
		auxiliar = tablero1;
		tablero1.reiniciar();
		assertEquals(tablero1, auxiliar);
	}

}
