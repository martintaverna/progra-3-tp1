package negocio;

import java.util.Arrays;
import java.util.Random;

public class Tablero {

	private boolean[][] tablero;
	
	public Tablero()
	{
		tablero = new boolean[4][4];
		aleatorizarLuces();
	}
	
	private void aleatorizarLuces()
	{
		Random rand = new Random();
		int jugadas = 3;
		int x = 0;
		int y = 0;
		while (jugadas > 0)
		{
			x = rand.nextInt(4);
			y = rand.nextInt(4);
			this.jugada(x, y);
			jugadas --;
		}
	}
	
	public void jugada (int i, int j)
	{
		cambiarFoco(i, j);
		if (i == 0)
			cambiarFoco(i + 1, j);			
		else if (i == 3)
			cambiarFoco(i - 1, j);	
		else
		{	
			cambiarFoco(i - 1, j);
			cambiarFoco(i + 1, j);
		}
		
		if (j == 0)
			cambiarFoco(i, j + 1);	
		else if (j == 3)
			cambiarFoco(i, j - 1);
		else
		{	
			cambiarFoco(i, j - 1);
			cambiarFoco(i, j + 1);
		}		
	}
	
	private void cambiarFoco(int i, int j)
	{
		this.tablero[i][j] = !this.tablero[i][j];
	}
	
	public boolean gano ()
	{
		boolean bandera = true;
		
		for(int i = 0; i < this.tablero.length; i++)
			for(int j = 0; j < this.tablero.length; j++)
				if (this.tablero[i][j] == true)
					bandera = false;
		return bandera;
	}
	
	public void reiniciar()
	{
		this.aleatorizarLuces();
	}
	
	public boolean getValor(int i, int j)
	{
		return this.tablero[i][j];
	}
	
	public int getTama�o() 
	{
		return this.tablero.length;
	}
	
	public void setPosicion(int i, int j, boolean k)
	{
		this.tablero[i][j] = k;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(tablero);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tablero other = (Tablero) obj;
		return Arrays.deepEquals(tablero, other.tablero);
	}
}
